<?php

/* @var $this yii\web\View */

$this->title = 'Sequoia Home';
?>
<div class="site-index">
    <div style="min-height: 75vh">
        <div style="width: 100%; background-color: black">
            <h1 class="mob" style="text-align: center;padding-top: 2rem;padding-bottom: 3rem"><i class="fab fa-pagelines"></i>Sequoia</h1>
            <h1 class="top-header"><i class="fab fa-pagelines"></i>Sequoia</h1>
        </div>
        <div style="width: 100%; background-color: #fff5b7;padding-bottom: 3rem;">
            <h1 style="text-align: center;padding-top: 2rem; font-weight: bold">
                New way to listen music
            </h1>
            <h2 style="text-align: center;padding-top: 2rem; padding-bottom: 3rem">
                Enjoy ad-free music, a lot of artists, and more. 
            </h2>
            <img class="pulse_img" style="width: 15vw; margin-left: auto; margin-right: auto;display: block;margin-bottom: 3rem " src="https://i.scdn.co/image/ab671c3d0000f430f0d170dfe2d4f4e2ec3eadde">
        </div>
        <div class="animated_div" style="width: 100%;padding-top: 7rem; background-color: #ff449f; background: url('https://content-tooling.spotifycdn.com/images/af63e367-ad10-4063-a8e2-5a50ae2a4b28_lie_circles-mobile.svg'), rgb(41, 65, 171); background-position: 65% 55%; height: 50vh">
            <h1 style="text-align: center; font-weight: bold; color: #fff5b7; vertical-align: center; background: black;">
                Get music for free
            </h1>
            <h2 style="text-align: center;padding-top: 2rem; padding-bottom: 3rem;color: #fff5b7; vertical-align: center">
                Enjoy ad-free music, a lot of artists, and even more. 
            </h2>
        </div>
    </div>
</div>
