<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about" style="">
    <h1 style='color:#ff449f;background-color: black;padding-top: 2rem; padding-bottom: 3rem;'><?= Html::encode($this->title) ?></h1>
    <div class="container">
        <div class='col-lg-3 card' style="margin: 1rem">
		   	<h2>About Us</h2>
		   	<h4>
			With Spotify, it’s easy to find the right music or podcast for every moment – on your phone, your computer, your tablet and more.

			There are millions of tracks and episodes on Spotify. So whether you’re behind the wheel, working out, partying or relaxing, the right music or podcast is always at your fingertips. Choose what you want to listen to, or let Spotify surprise you.

			You can also browse through the collections of friends, artists, and celebrities, or create a radio station and just sit back.

			Soundtrack your life with Spotify. Subscribe or listen for free.
			</h4>
    	</div>
    	<div class="col-lg-8 card">
    		
			<h2>Customer Service and Support</h2>
			<h3>Skip to next section</h3>
			<h4>
			    Help site. Check out our help site for answers to your questions and to learn how to get the most out of Spotify and your music.
			    Community. Get fast support from expert Spotify users. If there isn't already an answer there to your question, post it and someone will quickly answer. You can also suggest and vote on new ideas for Spotify or simply discuss music with other fans.
			    Contact us. Contact our Customer Support if you don't find a solution on our support site or Community.

    		</h4>
    	</div>
    	<div class="col-lg-12 card" style="margin-bottom: 3rem;">
    		<h2>
    			Or pick a topic:
    		</h2>
    		<h3>
			    Advertising on Spotify? Advertisers section
			    Press query? Press section
			    Applying for a job? Jobs section
    		</h3>
    		<p>
				Spotify USA, Inc. provides the Spotify service to users in the United States. Spotify AB provides the Spotify service to users in all other markets. 
    		</p>
    	</div>
    </div>
</div>
