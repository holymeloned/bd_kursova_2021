<div style="width: 100%; background-color: black">
    <h1 class="mob" style="text-align: center;padding-top: 2rem;padding-bottom: 3rem; color: #ff449f"><i class="fab fa-pagelines"></i>Browse</h1>
    <h1 class="top-header"><i class="fab fa-pagelines"></i>Browse</h1>
</div>
<div class="container">
	<input style="width: 100%;margin-top:1rem;margin-bottom: 1rem" type="text" name="title" id="search_input">
	<script type="text/javascript">
		function search(){
			let search_input = document.getElementById('search_input').value;
			location.href = '/song/search?title='+search_input;
		}
	</script>
	<button class="btn btn-warning" onclick="search()">Search</button>
	<?php if (count($model)): ?>
		<?php foreach ($model as $item): ?>
			<div class="card" style="text-align: center;">
				<h3>
					<?php
					$link = mysqli_connect("localhost", "root", "root", "sequoia_db");

					$sql = 'SELECT * FROM song WHERE id = '.$item;
					$result = mysqli_query($link, $sql);
					$song = mysqli_fetch_array($result);
					$sql = 'SELECT username FROM user WHERE id = '.$song['artist'];
					$result = mysqli_query($link, $sql);
					$artist_r = mysqli_fetch_array($result);
					echo $artist_r['username'].' - '.$song['title'];
					?>
				</h3>
				<p style="color: #00ead3">
					<?php
					$link = mysqli_connect("localhost", "root", "root", "sequoia_db");
					$sql = 'SELECT genre_name FROM genre WHERE id = '.$song['genre'];
					$result = mysqli_query($link, $sql);
					$tmp = mysqli_fetch_array($result);
					echo '#'.$tmp['0']
					 ?>	
				 </p>
				<audio controls src="<?php echo Yii::$app->request->baseUrl; ?>/storage/<?php echo $song['id'] ?>.mp3">
			    </audio>
			</div>
		<?php endforeach ?>
		<?php else: ?>
			<div class="card"><h1>No result</h1></div> 
	<?php endif; ?>
</div>