<?php 

namespace app\controllers;

use yii\web\Controller;
use app\models\Album;
use app\models\Song;
use app\models\Genre;

class AlbumController extends Controller
{
	public function actionIndex()
	{
		$genres = Genre::find()->all();
		$model = Album::find()->all();
		return $this->render('index',['model'=>$model],['genre'=>$genre]);
	}
}