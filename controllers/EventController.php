<?php 

namespace app\controllers;

use yii\web\Controller;
use app\models\Event;

class EventController extends Controller
{
	public function actionIndex()
	{
		$model = Event::find()->all();
		return $this->render('index',['model'=>$model]);
	}
}