<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "song".
 *
 * @property int $id
 * @property string|null $title
 * @property int|null $artist
 * @property int|null $album
 * @property string|null $release_date
 * @property int|null $genre
 *
 * @property Genre $genre0
 * @property Album $album0
 */
class Song extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'song';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['artist', 'album', 'genre'], 'integer'],
            [['release_date'], 'safe'],
            [['title'], 'string', 'max' => 255],
            [['genre'], 'exist', 'skipOnError' => true, 'targetClass' => Genre::className(), 'targetAttribute' => ['genre' => 'id']],
            [['album'], 'exist', 'skipOnError' => true, 'targetClass' => Album::className(), 'targetAttribute' => ['album' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'artist' => 'Artist',
            'album' => 'Album',
            'release_date' => 'Release Date',
            'genre' => 'Genre',
        ];
    }

    /**
     * Gets query for [[Genre0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGenre0()
    {
        return $this->hasOne(Genre::className(), ['id' => 'genre']);
    }

    /**
     * Gets query for [[Album0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAlbum0()
    {
        return $this->hasOne(Album::className(), ['id' => 'album']);
    }
}
