<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "album".
 *
 * @property int $id
 * @property string $title
 * @property int $artist
 * @property string $release_date
 * @property int $genre
 *
 * @property Genre $genre0
 * @property Song[] $songs
 */
class Album extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'album';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'artist', 'release_date', 'genre'], 'required'],
            [['artist', 'genre'], 'integer'],
            [['title'], 'string', 'max' => 255],
            [['release_date'], 'string'],
            [['genre'], 'exist', 'skipOnError' => true, 'targetClass' => Genre::className(), 'targetAttribute' => ['genre' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'artist' => 'Artist',
            'release_date' => 'Release Date',
            'genre' => 'Genre',
        ];
    }

    /**
     * Gets query for [[Genre0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGenre0()
    {
        return $this->hasOne(Genre::className(), ['id' => 'genre']);
    }

    /**
     * Gets query for [[Songs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSongs()
    {
        return $this->hasMany(Song::className(), ['album' => 'id']);
    }
}
