<?php
namespace app\models;

use yii\base\Model;
use yii\web\UploadedFile;

class UploadForm extends Model
{
    /**
     * @var UploadedFile
     */
    public $songFile;

    public function rules()
    {
        return [
            [['songFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'mp3'],
        ];
    }
    
    public function upload($id)
    {
        if ($this->validate()) {
            $this->songFile->saveAs('storage/' . $id . '.' . $this->songFile->extension);
            return $this->redirect(['index']);
        } else {
            return false;
        }
    }
}